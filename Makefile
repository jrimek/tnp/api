install:
	go get github.com/aws/aws-lambda-go/events
	go get github.com/aws/aws-lambda-go/lambda

build: 

	@for dir in `ls handler`; do \
			GOOS=linux go build -o handler/$$dir/main gitlab.com/jrimek/tnp/api/handler/$$dir; \
	done

run: build
	sam local start-api

package: build
	sam package \
		--template-file template.yaml \
		--output-template-file packaged.yaml \
		--s3-bucket learningsam

deploy: package
	sam deploy \
		--template-file packaged.yaml \
		--stack-name tnp \
		--capabilities CAPABILITY_IAM

describe:
		@aws cloudformation describe-stacks \
			--region eu-central-1 \
			--stack-name tnp

