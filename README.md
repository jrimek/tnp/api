# Dynamo DB access pattern

#### picture:

````json
{
  "gsi_1_pk": "Ferne_t",
  "gsi_1_sk_2_pk": 34,
  "gsi_2_sk": "Galerie_t_Ferne",
  "gsi_3_pk": "picture",
  "gsi_4_pk": "no",
  "gsi_4_sk": "no description",
  "pk": "p-00733d4e-d928-48a0-a2bc-e4184b6882c2",
  "title": "Skoga - Foss, Südisland 2010",
  "url": "https://d3r0swz0tt5vrb.cloudfront.net/DSC_2138W.jpg"
}
````

- all pictures
  - query gsi_3_pk for __picture__
- a picture
  - query pk for __uuid__
- front page pictures
  - query gsi_4_pk for __front-page__
- active pictures in category
  - query gsi_1_pk for __t_CATEGORY_NAME__
- first picture of each category
  - query gsi_1_sk_2_pk for __0__ AND gsi_2_sk for begins_with __CATEGORY_TYPE_NAME__


#### category:

````json
{
  "gsi_1_pk": "Mensch",
  "gsi_3_pk": "category",
  "gsi_4_pk": "Galerie",
  "pk": "c-b80b920f-ea87-4af5-8ed9-212b5d6b9bc1"
}
````

- all categories of category_type
  - query gsi_4_pk for __CATEGORY_TYPE_NAME__

# Deployment:

`make deploy`

I honestely already forgot how I deployed the javascript lambda function, it's definitly not part of the makefile
