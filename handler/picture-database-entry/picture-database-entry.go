package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"git-codecommit.eu-central-1.amazonaws.com/wowmate-io/src/awsh"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-xray-sdk-go/xray"
	"gitlab.com/jrimek/tnp/api/src/ddbb"

	uuid "github.com/satori/go.uuid"
)

//sess is a global variable to reuse the connection and make lambda faster
var sess *session.Session

func handler(ctx context.Context, s3Event events.S3Event) (string, error) {
	for _, record := range s3Event.Records {
		s3 := record.S3
		log.Printf("[%s - %s] Bucket = %s, Key = %s aws region = %s", record.EventSource, record.EventTime, s3.Bucket.Name, s3.Object.Key, record.AWSRegion)
		createDatabaseEntry(ctx, s3.Object.Key)
	}
	return "probably successfull", nil
}

func createDatabaseEntry(ctx context.Context, item string) {
	h := make(map[string]string)
	h["Access-Control-Allow-Origin"] = "*"
	dbname := os.Getenv("DYNAMODB_NAME")
	svc := dynamodb.New(sess)
	xray.AWS(svc.Client)

	uuidv4, err := uuid.NewV4()
	if err != nil {
		log.Println("Failed to create a new uuid V4")
		log.Println(err.Error())
		return
	}
	/*
		ActiveCategory     string `json:"gsi_1_pk"`
		Order              int    `json:"gsi_1_sk_2_pk"`
		ActiveCategoryType string `json:"gsi_2_sk"`
		Type               string `json:"gsi_3_pk"`
		FrontPage          string `json:"gsi_4_pk"`
		Description        string `json:"gsi_4_sk"`
		Title              string `json:"title"`
		URL                string `json:"url"`
	*/
	newPic := ddbb.Picture{
		UUID:               fmt.Sprintf("p-%v", uuidv4),
		ActiveCategory:     "keine_Kategorie_f",
		Order:              99,
		ActiveCategoryType: "_f_keine Kategorie",
		Type:               "picture",
		FrontPage:          "no",
		Description:        "a",
		Title:              "neues Bild",
		URL:                fmt.Sprintf("https://d3r0swz0tt5vrb.cloudfront.net/%v", item),
	}

	av, err := dynamodbattribute.MarshalMap(newPic)
	if err != nil {
		log.Println("Got error marshalling map:")
		log.Println(err.Error())
		return
	}

	input := &dynamodb.PutItemInput{
		Item:                   av,
		ReturnConsumedCapacity: aws.String("TOTAL"),
		TableName:              aws.String(dbname),
	}

	oup, err := svc.PutItemWithContext(ctx, input)
	if err != nil {
		log.Printf("Got error calling PutItem: %v", err)
		return
	}

	log.Printf("Consumed WCU: %f: ", *oup.ConsumedCapacity.CapacityUnits)

	return
}

func init() {
	awsh.InitLogging()
}

func main() {
	sess = awsh.InitSession()
	lambda.Start(handler)
}
