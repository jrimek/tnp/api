package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"strings"

	"git-codecommit.eu-central-1.amazonaws.com/wowmate-io/src/awsh"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-xray-sdk-go/xray"
	"gitlab.com/jrimek/tnp/api/src/ddbb"
)

//sess is a global variable to reuse the connection and make lambda faster
var sess *session.Session

func handler(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	h := make(map[string]string)
	h["Access-Control-Allow-Origin"] = "*"
	ddbName := os.Getenv("DYNAMODB_NAME")
	bucketName := os.Getenv("BUCKET_NAME")
	svc := dynamodb.New(sess)
	xray.AWS(svc.Client)

	input := &dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"pk": {
				S: aws.String(req.PathParameters["uuid"]),
			},
		},
		ReturnConsumedCapacity: aws.String("TOTAL"),
		ReturnValues:           aws.String("ALL_OLD"),
		TableName:              aws.String(ddbName),
	}

	output, err := svc.DeleteItemWithContext(ctx, input)
	if err != nil {
		log.Printf("Got error calling PutItem: %v", err)
		return events.APIGatewayProxyResponse{
			StatusCode: 500,
			Headers:    h,
			Body:       "",
		}, nil
	}

	wcuConsumed := *output.ConsumedCapacity.CapacityUnits
	log.Printf("Consumed WCU: %f: ", wcuConsumed)

	pic := ddbb.Picture{}
	err = dynamodbattribute.UnmarshalMap(output.Attributes, &pic)

	picName := strings.Replace(pic.URL, "https://d3r0swz0tt5vrb.cloudfront.net/", "", 1)
	log.Println(picName)

	svc2 := s3.New(sess)

	_, err = svc2.DeleteObject(&s3.DeleteObjectInput{Bucket: aws.String(bucketName), Key: aws.String(picName)})
	if err != nil {
		log.Printf("Failed to delete picture: %v", err)
		return events.APIGatewayProxyResponse{
			StatusCode: 500,
			Headers:    h,
			Body:       "",
		}, nil
	}

	err = svc2.WaitUntilObjectNotExists(&s3.HeadObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(picName),
	})
	if err != nil {
		log.Printf("Failed while waiting for delete: %v", err)
		return events.APIGatewayProxyResponse{
			StatusCode: 500,
			Headers:    h,
			Body:       "",
		}, nil
	}

	return events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Headers:    h,
		Body:       "",
	}, nil
}

func init() {
	awsh.InitLogging()
}

func main() {
	sess = awsh.InitSession()
	lambda.Start(handler)
}
