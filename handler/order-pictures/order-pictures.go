package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"os"

	"git-codecommit.eu-central-1.amazonaws.com/wowmate-io/src/awsh"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-xray-sdk-go/xray"
	"gitlab.com/jrimek/tnp/api/src/ddbb"
)

//sess is a global variable to reuse the connection and make lambda faster
var sess *session.Session

func handler(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	h := make(map[string]string)
	h["Access-Control-Allow-Origin"] = "*"
	dbname := os.Getenv("DYNAMODB_NAME")
	svc := dynamodb.New(sess)
	xray.AWS(svc.Client)

	var extPics []ddbb.ExtPic
	err := json.Unmarshal([]byte(req.Body), &extPics)
	if err != nil {
		log.Println("Got error unmarshalling the json into the ExtPic struct:")
		log.Println(err.Error())
		return events.APIGatewayProxyResponse{
			StatusCode: 500,
			Headers:    h,
			Body:       "",
		}, nil
	}
	var pics []ddbb.Picture
	var wcuConsumed float64

	for i, ep := range extPics {
		pic := ddbb.FormatPic(ep)
		pic.Order = i

		pics = append(pics, pic)
	}

	for _, pic := range pics {
		av, err := dynamodbattribute.MarshalMap(pic)
		if err != nil {
			log.Println("Got error marshalling map:")
			log.Println(err.Error())
			return events.APIGatewayProxyResponse{
				StatusCode: 500,
				Headers:    h,
				Body:       "",
			}, nil
		}

		input := &dynamodb.PutItemInput{
			Item:                   av,
			ReturnConsumedCapacity: aws.String("TOTAL"),
			TableName:              aws.String(dbname),
		}

		oup, err := svc.PutItemWithContext(ctx, input)
		if err != nil {
			log.Printf("Got error calling PutItem: %v", err)
			return events.APIGatewayProxyResponse{
				StatusCode: 500,
				Headers:    h,
				Body:       "",
			}, nil
		}
		wcuConsumed = wcuConsumed + *oup.ConsumedCapacity.CapacityUnits
	}

	log.Printf("Consumed WCU: %f: ", wcuConsumed)

	return events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Headers:    h,
		Body:       "",
	}, nil
}

func init() {
	awsh.InitLogging()
}

func main() {
	sess = awsh.InitSession()
	lambda.Start(handler)
}
