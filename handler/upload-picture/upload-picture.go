package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"git-codecommit.eu-central-1.amazonaws.com/wowmate-io/src/awsh"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/sirupsen/logrus"
)

//sess is a global variable to reuse the connection and make lambda faster
var sess *session.Session

func handler(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	svc := s3.New(sess)
	//xray.AWS(svc.Client)

	r, _ := svc.PutObjectRequest(&s3.PutObjectInput{
		ContentType: aws.String("image/jpeg"),
		Bucket:      aws.String("torsten-niese-pictures"),
		Key:         aws.String("thisisfine.jpg"),
	})

	str, err := r.Presign(15 * time.Minute)
	if err != nil {
		logrus.Printf("Failed to pre sign the S3 link: %v", err)
		return events.APIGatewayProxyResponse{}, nil
	}

	h := make(map[string]string)
	h["Access-Control-Allow-Origin"] = "*"
	return events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Headers:    h,
		Body:       fmt.Sprintf("{\"url\": \"%v\"}", str),
	}, nil
}

func init() {
	awsh.InitLogging()
}

func main() {
	sess = awsh.InitSession()
	lambda.Start(handler)
}
