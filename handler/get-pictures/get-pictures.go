package main

import (
	"context"
	"fmt"
	"net/http"

	"git-codecommit.eu-central-1.amazonaws.com/wowmate-io/src/awsh"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"gitlab.com/jrimek/tnp/api/src/ddbb"
)

//sess is a global variable to reuse the connection and make lambda faster
var sess *session.Session

func handler(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	qi := &dynamodb.QueryInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":v1": {
				S: aws.String("picture"),
			},
		},
		KeyConditionExpression: aws.String("gsi_3_pk = :v1"),
		IndexName:              aws.String("GSI_3"),
	}

	s, err := ddbb.QueryDynamoDB(ctx, qi, sess)
	if err != nil {
		return events.APIGatewayProxyResponse{}, fmt.Errorf(http.StatusText(http.StatusInternalServerError))
	}

	r := ddbb.FormatExtPic(s)
	return ddbb.FormResponse(r)

}

func init() {
	awsh.InitLogging()
}
func main() {
	sess = awsh.InitSession()
	lambda.Start(handler)
}
