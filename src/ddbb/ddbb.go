package ddbb

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-xray-sdk-go/xray"
	uuid "github.com/satori/go.uuid"

	log "github.com/sirupsen/logrus"
)

//Category holds all fields of a category db entry
type Category struct {
	UUID         string `json:"pk"`
	Category     string `json:"gsi_1_pk"`
	Type         string `json:"gsi_3_pk"`
	CategoryType string `json:"gsi_4_pk"`
}

//ExtCategory holds all fields of a category db entry
type ExtCategory struct {
	UUID         string `json:"uuid"`
	Category     string `json:"name"`
	CategoryType string `json:"type_name"`
}

//FormatExtCategory transforms the data from dynamodb to a format the frontend can handle
func FormatExtCategory(categories []Category) []ExtCategory {
	extCat := []ExtCategory{}

	for _, c := range categories {
		ec := ExtCategory{}
		ec.UUID = c.UUID
		ec.Category = c.Category
		ec.CategoryType = c.CategoryType
		extCat = append(extCat, ec)
	}
	return extCat
}

//FormatCategory transforms the data from dynamodb to a format the frontend can handle
func FormatCategory(ec ExtCategory) (c Category) {
	uuid, _ := uuid.NewV4()

	c.UUID = fmt.Sprintf("c-%v", uuid.String())
	c.Category = ec.Category
	c.CategoryType = ec.CategoryType
	c.Type = "category"
	return c
}

//Picture holds all fields of a picture db entry
type Picture struct {
	UUID               string `json:"pk"`
	ActiveCategory     string `json:"gsi_1_pk"`
	Order              int    `json:"gsi_1_sk_2_pk"`
	ActiveCategoryType string `json:"gsi_2_sk"`
	Type               string `json:"gsi_3_pk"`
	FrontPage          string `json:"gsi_4_pk"`
	Description        string `json:"gsi_4_sk"`
	Title              string `json:"title"`
	URL                string `json:"url"`
}

//ExtPic is the format the frontend recieves and expects
type ExtPic struct {
	UUID         string `json:"uuid"`
	Category     string `json:"category_name"`
	CategoryType string `json:"category_type_name"`
	Order        int    `json:"order"`
	FrontPage    string `json:"front_page"`
	Description  string `json:"description"`
	Title        string `json:"title"`
	URL          string `json:"url"`
	Active       string `json:"active"`
}

//FormatPic transforms the data from the frontend format to the dynamodb one
func FormatPic(ep ExtPic) Picture {
	p := Picture{}

	p.UUID = ep.UUID
	p.ActiveCategory = fmt.Sprintf("%v_%v", ep.Category, ep.Active)
	p.Order = ep.Order
	p.ActiveCategoryType = fmt.Sprintf("%v_%v_%v", ep.CategoryType, ep.Active, ep.Category)
	p.Type = "picture"
	p.FrontPage = ep.FrontPage
	p.Description = ep.Description
	p.Title = ep.Title
	p.URL = ep.URL

	return p
}

//FormatExtPic transforms the data from dynamodb to a format the frontend can handle
func FormatExtPic(pictures []Picture) []ExtPic {
	extPic := []ExtPic{}

	for _, p := range pictures {
		ep := ExtPic{}
		c, a := SplitActiveCategory(p.ActiveCategory)
		ep.UUID = p.UUID
		ep.Category = c
		ep.CategoryType = ExtractCategoryType(p.ActiveCategoryType)
		ep.Order = p.Order
		ep.Description = p.Description
		ep.FrontPage = p.FrontPage
		ep.Title = p.Title
		ep.URL = p.URL
		ep.Active = a
		extPic = append(extPic, ep)
	}
	return extPic
}

//SplitActiveCategory splits the gsi_1_pk for pictures (e.g. Ferne_t) in two strings
//so they can be returned seperately to the frontend
func SplitActiveCategory(s string) (category string, active string) {
	a := strings.Split(s, "_")

	return a[0], a[1]
}

//ExtractCategoryType splits the gsi_2_sk for pictures (e.g. Galerie_t_Ferne) to get the first part
func ExtractCategoryType(s string) string {
	a := strings.Split(s, "_")

	return a[0]
}

//PKQuery is a helper function to only query the partition key
func PKQuery(pkName string) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":v1": {
				S: aws.String(pkName),
			},
		},
		KeyConditionExpression: aws.String("pk = :v1"),
	}

	return input
}

//QueryDynamoDB is a helper that takes a query input and returns a struct
func QueryDynamoDB(ctx context.Context, qi *dynamodb.QueryInput, sess *session.Session) ([]Picture, error) {
	svc := dynamodb.New(sess)
	xray.AWS(svc.Client)

	result, err := query(ctx, svc, qi)
	if err != nil {
		log.WithFields(log.Fields{
			"http_code": strconv.Itoa(http.StatusInternalServerError),
		}).Error("Failed to query DynamoDB: " + err.Error())
		return nil, err
	}

	summaries := []Picture{}
	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &summaries)
	if err != nil {
		log.WithFields(log.Fields{
			"http_code": strconv.Itoa(http.StatusInternalServerError), //500
		}).Error("Failed to unmarshal DynamoDB response: " + err.Error())
		return nil, err
	}
	return summaries, nil
}

//CategoryQueryDynamoDB is a helper that takes a query input and returns a struct
func CategoryQueryDynamoDB(ctx context.Context, qi *dynamodb.QueryInput, sess *session.Session) ([]Category, error) {
	svc := dynamodb.New(sess)
	xray.AWS(svc.Client)

	result, err := query(ctx, svc, qi)
	if err != nil {
		log.WithFields(log.Fields{
			"http_code": strconv.Itoa(http.StatusInternalServerError),
		}).Error("Failed to query DynamoDB: " + err.Error())
		return nil, err
	}

	summaries := []Category{}
	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &summaries)
	if err != nil {
		log.WithFields(log.Fields{
			"http_code": strconv.Itoa(http.StatusInternalServerError), //500
		}).Error("Failed to unmarshal DynamoDB response: " + err.Error())
		return nil, err
	}
	return summaries, nil
}

func query(ctx context.Context, svc *dynamodb.DynamoDB, qi *dynamodb.QueryInput) (*dynamodb.QueryOutput, error) {
	qi.TableName = aws.String(os.Getenv("DYNAMODB_NAME"))
	qi.ReturnConsumedCapacity = aws.String("TOTAL")

	result, err := svc.QueryWithContext(ctx, qi)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case dynamodb.ErrCodeProvisionedThroughputExceededException:
				log.WithFields(log.Fields{
					"dynamodb_error_code": dynamodb.ErrCodeProvisionedThroughputExceededException,
				}).Error(aerr.Error())
			case dynamodb.ErrCodeResourceNotFoundException:
				log.WithFields(log.Fields{
					"dynamodb_error_code": dynamodb.ErrCodeResourceNotFoundException,
				}).Error(aerr.Error())
			case dynamodb.ErrCodeInternalServerError:
				log.WithFields(log.Fields{
					"dynamodb_error_code": dynamodb.ErrCodeInternalServerError,
				}).Error(aerr.Error())
			default:
				log.WithFields(log.Fields{
					"dynamodb_error_code": "defaulted aerr",
				}).Error(aerr.Error())
			}
		} else {
			log.WithFields(log.Fields{
				"dynamodb_error_code": "else err",
			}).Error(err.Error())
		}
		return nil, err
	}

	log.WithFields(log.Fields{
		"rcu": fmt.Sprintf("%f", *result.ConsumedCapacity.CapacityUnits),
	}).Info("Consumed RCU")

	return result, nil
}

//FormResponse is a helper function to create an APIGateway response
//
//if err is not null they somehow throw away the apigatewayproxyresponse object, including
//the eventual header, which might lead to CORS problems..
func FormResponse(summaries []ExtPic) (events.APIGatewayProxyResponse, error) {
	if len(summaries) == 0 {
		log.WithFields(log.Fields{
			"http_code": strconv.Itoa(http.StatusNotFound),
		}).Error("Empty response from DynamoDB")
		return events.APIGatewayProxyResponse{}, fmt.Errorf(http.StatusText(http.StatusNotFound))
	}

	js, err := json.Marshal(summaries)
	if err != nil {
		log.WithFields(log.Fields{
			"http_code": strconv.Itoa(http.StatusInternalServerError),
		}).Error("Failed to marshal JSON: " + err.Error())
		return events.APIGatewayProxyResponse{}, fmt.Errorf(http.StatusText(http.StatusInternalServerError))
	}

	h := make(map[string]string)
	h["Access-Control-Allow-Origin"] = "*"

	return events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Headers:    h,
		Body:       string(js),
	}, nil
}

//FormCategoryResponse is a helper function to create an APIGateway response
func FormCategoryResponse(summaries []ExtCategory) (events.APIGatewayProxyResponse, error) {
	if len(summaries) == 0 {
		log.WithFields(log.Fields{
			"http_code": strconv.Itoa(http.StatusNotFound),
		}).Error("Empty response from DynamoDB")
		return events.APIGatewayProxyResponse{}, fmt.Errorf(http.StatusText(http.StatusNotFound))
	}

	js, err := json.Marshal(summaries)
	if err != nil {
		log.WithFields(log.Fields{
			"http_code": strconv.Itoa(http.StatusInternalServerError),
		}).Error("Failed to marshal JSON: " + err.Error())
		return events.APIGatewayProxyResponse{}, fmt.Errorf(http.StatusText(http.StatusInternalServerError))
	}

	h := make(map[string]string)
	h["Access-Control-Allow-Origin"] = "*"

	return events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Headers:    h,
		Body:       string(js),
	}, nil
}
